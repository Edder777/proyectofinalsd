import com.rabbitmq.client.*;


import java.util.Scanner; 

import java.io.*;

import java.nio.charset.StandardCharsets;

public class moduloProductor {

    private final static String QUEUE_NAME = "cola";

    public static void main(String[] argv) throws Exception , InterruptedException{
    	
    	ConnectionFactory factory = new ConnectionFactory();
    	 factory.setHost("10.0.2.15");
         factory.setUsername("admin");
         factory.setPassword("admin");
       // factory.setVirtualHost("/");
        //factory.setHost("localhost");
       // factory.setPort(15672);

         String nombreFichero = "texto.txt";
        
         BufferedReader br = null;
         try {
    
            br = new BufferedReader(new FileReader(nombreFichero));
       
            String texto = br.readLine();
            int cont=0; 
            int contn=0;
            while(texto != null)
            {
                
                System.out.println("Cargando trama con mensaje: "+texto);
                
                try (Connection connection = factory.newConnection();
                        Channel channel = connection.createChannel()) {
                       channel.queueDeclare(QUEUE_NAME, false, false, false, null);
                       int contador= cont+1;
                       System.out.println("Leyendo linea: "+contador);
                       String message = texto;
                       
                       if (texto.isEmpty()) {
                    	System.out.println("");
						System.out.println("Error en trama: no hay contenido para enviar");
						System.out.println("");
						contn++;
                       }else {
						
						 try {
	                    	   Thread.sleep(125);
						} finally {
							channel.basicPublish("", QUEUE_NAME, null, message.getBytes(StandardCharsets.UTF_8));
						}
						
	                       System.out.println("Trama '" + message + "' Enviada a la cola "+QUEUE_NAME+"!!");
	                       System.out.println("");
	                       cont++;
	                   }
					}catch(Exception e){
	                	   System.out.println(e.getMessage());
	                 }
             
                      
                
                texto = br.readLine();
            }
            if (contn>0) {
            	System.out.println(cont+" Tramas enviadas");
            	System.out.println(contn+" Trama(s) No enviada(s) por falta de contenido.");
			}else {
				System.out.println(cont+" Tramas enviadas");
			}
           
         }catch (FileNotFoundException e) {
             System.out.println("Error: Fichero no encontrado");
             System.out.println(e.getMessage());
         }
         catch(Exception e) {
             System.out.println("Error de lectura del fichero");
             System.out.println(e.getMessage());
         }
         finally {
             try {
                 if(br != null)
                     br.close();
             }
             catch (Exception e) {
                 System.out.println("Error al cerrar el fichero");
                 System.out.println(e.getMessage());
             }
         }
         
       
        
        
       
    }
}