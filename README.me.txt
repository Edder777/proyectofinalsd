Nombre: Edder Parra y Juan Castañeda

	Descripción del Sistema:
	
	Modulo "moduloProductor.java":

	Es aplicativo se encarga de leer un documento de texto de extensión .txt
        y enviar por cada linea una trama a la cola llamada: "cola". El envío se realiza
	cada medio segundo mendiente el uso del metodo Thread.sleep() lo que impide
	conflictos con las tareas en los modulos consumidores.
	
	Modulo "Consumidor.java"

	Se encarga establer una conexión remota con el servidor middleware RabbitMQ, consumir
	las tareas de la cola llamada "cola" y efectuar un conteo de caracteres por trama, escribiendo 
	finalmente un archivo .json, en cual contine la cantidad de lineas procesadas del archivo "texto.txt" 
	
	Instrucciones: 

	1.- Instalar java y Gitlab

	2.- Clonar el repositorio desde la direccion: https://gitlab.com/Edder777/proyectofinalsd.git en el directorio de su preferencia 

	3.- Abrir una terminal en el directorio donde esta ubicado el repositorio clonado 

	4.- Dentro encontrara los siguientes archivos : 
		1.- Reporte.json 
		2.- moduloProductor.java 
		3.- Consumidor.java
		4.- json.jar
		5.- slf4j-api-1.7.26.jar
		6.- slf4j-simple-1.7.26.jar
		7.- amqp-client-5.7.1.jar
		8.- texto.txt
		9.- Readme.me

	5.- Ejecutar la siguiente linea de comandos para levantar los modulos "Consumidor.java" : 
		java -cp .:amqp-client-5.7.1.jar:slf4j-api-1.7.26.jar:slf4j-simple-1.7.26.jar:json.jar Consumidor

	
	6.- Ejecutar la siguiente linea de comandos para levantar el modulo "moduloProductor.java" :
	java -cp .:amqp-client-5.7.1.jar:slf4j-api-1.7.26.jar:slf4j-simple-1.7.26.jar:json.jar moduloProductor.java
	
	El moduloProductor.java lee las lineas del archivo llamado: texto.txt el cual posee 1000 lineas.

	7.-  Una vez terminada la ejecucion del "moduloProductor.java" Revisar el archivo Reporte.json tras cada ejecucion de la aplicacion.
	
	